# ТЗ8 Методы стохастической оптимизации

Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине Оптимизационные задачи в машинном обучении. Для ознакомления с поставленной задачей нажмите [читать tz8_stochastic_optimization.pdf](illustrations/tz8_stochastic_optimization.pdf)

## Демонстрация возможностей

Множество примеров работы модуля доступны к просмотру любым удобным для Вас способом. <br/>
* Онлайн Google Colab Notebook - нажмите [открыть Google Colab](https://drive.google.com/file/d/1iZ9-op5hav_UpNmR9Zi1SQVAHSHZLL3m/view?usp=sharing)
* Оффлайн Jupyter Notebook - нажмите [скачать tz8_stochastic_optimization.ipynb](tz8_stochastic_optimization.ipynb)

## Иллюстрация работы

* Решение и вывод <br/>
![Output](illustrations/output.png) <br/>
* Продвинутая визуализация <br/>
![Visualization](illustrations/visualization.gif) <br/>

## Подготовка окружения

```
# Для визуализации необходим CUDA
!nvidia-smi
!pip install GPUtil
```

## Импорт модуля

```
# Клонирование репозитория
!git clone https://leonidalekseev:maCRxpzWvEcohHKKTeG9@gitlab.com/optimization-tasks-in-machine-learning/tz8_stochastic_optimization.git
# Импорт всех функций из модуля
from tz8_stochastic_optimization.utils import *
```

## Документация функций

`help(set_function)`

```
Help on function set_function in module tz8_stochastic_optimization.utils:

set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
    Установка объекта sympy функции из строки.
    Количество переменных проверяется. Отображение функции настраивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    functions_symbols: tuple, optional
        Переменные функции
    functions_title: str
        Заголовок для отображения
    functions_designation: str
        Обозначение для отображения
    is_display_input: bool
        Отображать входную функцию или нет
    _is_system: bool
        Вызов функции программой или нет
    
    Returns
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    investigated_function: sympy
        Исследуемая функция
    functions_symbols: tuple
        Переменные функции
```

`help(visualize_plotly)`

```
Help on function visualize_plotly in module tz8_stochastic_optimization.utils:

visualize_plotly(X: Union[list, numpy.ndarray], y: Union[list, numpy.ndarray], ground_truth: Union[list, numpy.ndarray], predictions: Union[list, numpy.ndarray], is_display_input: bool = True, start: Union[int, float] = -1, stop: Union[int, float] = 1, detail: Union[int, float] = 100, contour: Union[tuple, NoneType] = None) -> None
    Визуализация полученной функции с предикторами. 
    
    Parameters
    ===========
    
    X: list, numpy.ndarray
        Массив кординат по x
    y: list, numpy.ndarray
        Массив кординат по y
    ground_truth: list, numpy.ndarray
        Массив истинных предсказаний
    predictions: list, numpy.ndarray
        Массив предсказаний модели
    is_display_input: bool
        Отображать входную функцию или нет
    start: int, float
        Начало графика
    stop: int, float
        Конец графика
    detail: int, float
        Детализация графика
    contour: tuple, optional
        Параметры контура
```

`help(classification)`

```
Help on function classification in module tz8_stochastic_optimization.utils:

classification(X_train: list, y_train: list, X_test: list, y_test: list, method: Union[int, str] = 0, C_regularisation: float = 0.1, max_iterations: int = 1000, batch_size: int = 50, learning_rate: list = [0.1, 0.001], loss_function: str = 'hinge', is_display_input: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
    Классификация с заданными параметрами. 
    
    Parameters
    ===========
    
    X_train: list, numpy.ndarray
        Массив обучающей выборки
    y_train: list, numpy.ndarray
        Массив предсказываемой переменной
    X_test: list, numpy.ndarray
        Массив тестовой выборки
    y_test: list, numpy.ndarray
        Массив предсказываемой переменной
    method: int, str
        Методы классификации
        0, 'svm_sgd': методом опорных векторов с 
            использованием градиентного спуска
    C_regularisation: float
        Параметр регуляризации
    max_iterations: int = 1000
        Максимум итераций
    batch_size: int = 50
        Размер батча
    learning_rate: list = [1e-1,1e-3]
        Скорость обучения
    loss_function: str = "hinge"
        Функция ошибки
        'hinge': функция ошибки шарнира
        'sq_hinge': квадратичная функция ошибки шарнира
        'logistic': логистическая функция ошибки
    is_display_input: bool = False
        Отображать входные данные или нет
    is_display_conclusion: bool = True
        Отображать вывод или нет
    is_try_visualize: bool = False
        Визуализация процесса
    
    Returns
    ===========
    
    predictions: list
        Получившееся функция
    coefficients: list
        Массив коэффициентов
    free_term: float
        Свободный член
```

## Участники проекта

* [Быханов Никита](https://gitlab.com/BNik2001) - Менеджер проектa, Тестировщик
* [Алексеев Леонид](https://gitlab.com/LeonidAlekseev) - Программист, Тестировщик
* [Семёнова Полина](https://gitlab.com/polli_eee) - Аналитик, Тестировщик
* [Янина Марина](https://gitlab.com/marinatdd) - Аналитик, Тестировщик
* [Буркина Елизавета](https://gitlab.com/lizaburkina) - Аналитик
* [Егорин Никита](https://gitlab.com/hadesm8) - Аналитик

`Группа ПМ19-1`

## Используемые источники

* [Sympy documentation](https://www.sympy.org/en/index.html)
* [Plotly documentation](https://plotly.com/python/)

